# Background

As discussed, we want to start creating AWS CDK scripts to replace our manual deployment steps in each AWS account. This aligns with our goal to set up our infrastructure via a deployment pipeline.

# Spike

To start small, we’ve created a simple CDK project to deploy a Maintenance Page website in AWS.  The site is hosted in S3 and uses CloudFront to allow public access.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
