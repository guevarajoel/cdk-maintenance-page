import * as cdk from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import * as CdkMaintenancePage from '../lib/cdk-maintenance-page-stack';

describe('Template generation', () => {
  let template: Template;
  beforeAll(() => {
    const app = new cdk.App();
    const stack = new CdkMaintenancePage.CdkMaintenancePageStack(
      app,
      'MyTestStack'
    );
    template = Template.fromStack(stack);
  });

  test('Stack has an S3 bucket with expected properties', () => {
    template.hasResourceProperties('AWS::S3::Bucket', {
      PublicAccessBlockConfiguration: {
        BlockPublicAcls: true,
        BlockPublicPolicy: true,
        IgnorePublicAcls: true,
        RestrictPublicBuckets: true,
      },
      BucketName: 'maintenance-page-site',
    });
  });
  test('Stack has a CloudFront distribution with expected properties', () => {
    template.hasResourceProperties('AWS::CloudFront::Distribution', {
      DistributionConfig: {
        DefaultCacheBehavior: {
          AllowedMethods: ['GET', 'HEAD'],
          CachedMethods: ['GET', 'HEAD'],
          Compress: true,
          ForwardedValues: {
            Cookies: {
              Forward: 'none',
            },
            QueryString: false,
          },
          TargetOriginId: 'origin1',
          ViewerProtocolPolicy: 'redirect-to-https',
        },
        DefaultRootObject: 'index.html',
        Enabled: true,
        HttpVersion: 'http2',
        IPV6Enabled: true,
        Origins: [
          {
            ConnectionAttempts: 3,
            ConnectionTimeout: 10,
            DomainName: {
              'Fn::GetAtt': [
                'maintenancepagesite67382A39',
                'RegionalDomainName',
              ],
            },
            Id: 'origin1',
            S3OriginConfig: {
              OriginAccessIdentity: {
                'Fn::Join': [
                  '',
                  [
                    'origin-access-identity/cloudfront/',
                    {
                      Ref: 'OAIE1EFC67F',
                    },
                  ],
                ],
              },
            },
          },
        ],
        PriceClass: 'PriceClass_100',
        ViewerCertificate: {
          CloudFrontDefaultCertificate: true,
        },
      },
    });
  });
});
